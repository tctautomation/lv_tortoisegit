RSRC
 LVCCLBVW  �  ~      �      GitDriver.lvclass     � �  0          < � @�      ����            ��Sx�ݎD���Z��/�          �:oV��O��p�u����ُ ��	���B~       ,��p��G����o   ������ُ ��	���B~   Om�x���d�����          9 LVCC    VILB    PTH0      GitDriver.lvclass                "   x�c`c`j`�� Č@������ �3  h6     H  x�c`��� H1200��,h�`Ʀ&�e..����P7�BD�bL{�&�T���) >�n�{j(    VIDS       V  �x�`d`�4�0; ���X���!9?%���g��#L� �yZh����|������\���T���D�#�H����n���6`%YY�3����u#��Qa1<�P��P*|�фbK ���@y�<���Y�O; �~���Yz'�:Adg��qF1 ���1�.4�ú���b��w�w� ��;�@�U�
@�w��p]���.P��!�9@q����@�� W�����P�	*��wB�HznB�����0Hk@ً����@bO�t����@��@z��H@ٌ@K/@�lP�hl����.��aK� �q�     �   19.0.1       �   19.0    �   19.0.1       �   19.0    �   19.0.1                        n  j

Copyright (c) 2014, John Donaldson
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
1. Redistributions of source code must retain the above copyright
   notice, this list of conditions and the following disclaimer.
2. Redistributions in binary form must reproduce the above copyright
   notice, this list of conditions and the following disclaimer in the
   documentation and/or other materials provided with the distribution.
3. All advertising materials mentioning features or use of this software
   must display the following acknowledgement:
   This product includes software developed by the John Donaldson.
4. Neither the name of the John Donaldson nor the
   names of its contributors may be used to endorse or promote products
   derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY John Donaldson ''AS IS'' AND ANY
EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL John Donaldson BE LIABLE FOR ANY
DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
     ������  �  �@ �� !�� a�  !�� !�� !�� !�� !�� �� �� �� �� �� �� �� ��0��,�����$���.9��$�� ���,��0�@ �  �  ����   ���������������������������������                              ��                              ��        +                     ��       +��               �    ��      +����             ��    ��   ��V                   �    ��       ���               �    ��       ���               �    ��       ���               �    ��       ���               �    ��       ��� ��                 ��   ��V ���Vu�                 ��      +�P&Ju�+                ��     V��Jutu�+                ��      ����uv�+                ��      �����ʬ+                ��       ����V�+                ��   ��V ���  ++                ��       ���       ��+          ��       Ь�       ���+        ��       ���     ##���+      ��       ���       ����+    ��       ���       �����##  ��   V�V ���       ����+    ��               �����+      ��      +����      ���+        ��       +��       ��+          ��        +                     ��                              ��                              ���������������������������������          FPHP              �  .x��T�OA�ma��2� T1�p�b	�"���@�A�)�h0����(�'��i�	���!L��=h8x�C�W<x!̶��n[���0�L����o� ��Y�P�g��?C�� dc�/�� �4��i�m
�b�Gѣ��/�O��@��x̶�����m@��7��K9&�n�\e�zha�� �J�_tQK� hm�ꋑv:���䌪I���R4�q�;+�Z��?X��1a�mJ@�6X]]��|%PԲуa��;����~zNʝ�0n�:�e,�4������\gE�S�e[�W,\>�G�6�R��-��{K{H�>-f��6S�j���KX�oЮ��Rf����F+��r
]��R��í���!T<̝�H��Դ�������|7�<�0e��x2|*�\�Y=oll`ϻ��%-/��&�;L.u�� �<Y�ii��s�V�p��b���XB�{��6���#9�vt�ē�O�Uz#�LMN��몢�J-W�մ�����lr��>�Iͪ���c�E��-��W�u ^<� x����z��De�Tv����(x�$d��͕E�5A#&���KKKX���R1Xu�]:%�7����nƏ�f|��K�I�U;�7��U�H�f^��4��4�w@����j΅�+D3B4�p1�6v �6�K����{��n~��<a�������� �<t���e|��M;A��N�k��U�����ҫ m��vq�                  BDHP               b   rx�c``��`��P���I�+�!���YЏ�7���a �( 	����.��>��� �l���9�2-�����z�\�8Se�<� b            �      NI.LV.All.SourceOnly    �      !          !_ni_LastKnownOwningLVClassCluster   X �      0����      <    @2����GitExecutablePath @P   GitDriver.lvclass                   ;   (                                         �x��P�N�0����hAp��KPӢ�*J�D�0q�8��~�_)��)����ڝ�gg�o���X� �w�e��P=2���Ms�������m&U�2*��,�B-׏6�E�V�R��o�"fdȑ�����♙٪�5)$;�ag�� �<�Èkn�4�,�
Q�.���%��uB٪�� ҽ*~<��1&����~�}�hG��!�q����‿̯r�Gw�@�������!� �\�o�~L\      w       X      � �   a      � �   j      � �   s � �   � �   u� � �   � �Segoe UISegoe UISegoe UI00 RSRC
 LVCCLBVW  �  ~      �               4  h   LIBN      xLVSR      �RTSG      �CCST      �LIvi      �CONP      �TM80      �DFDS      LIds      VICD      ,vers     @GCPR      �STRG      �ICON      �icl8      �CPC2      �LIfp      FPEx      FPHb      0FPSE      DVPDP      XLIbd      lBDEx      �BDHb      �BDSE      �VITS      �DTHP      �MUID      �HIST      �VCTP      FTAB           ����                                   ����       �        ����       �        ����       �        ����              ����      $        ����      L        ����      �        ����      �       ����             ����             ����      (       	����      <       
����      L        ����      `        ����      t        ����      	�        ����      
l        ����      p        ����      x        ����      �        ����      �        ����      �        ����      �        ����      �        ����      �        ����      �        ����              ����               ����      �        ����      �        ����      �        ����             �����           SCCFileStatusEnum.ctl