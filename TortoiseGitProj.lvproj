﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="19008000">
	<Property Name="CCSymbols" Type="Str"></Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="GIT_FILE" Type="Bool">false</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Provider" Type="Folder">
			<Property Name="GIT_FILE" Type="Bool">false</Property>
			<Property Name="NI.SortType" Type="Int">3</Property>
			<Item Name="TortoiseGit_Global_Init.vi" Type="VI" URL="../TortoiseGit/TortoiseGit_Global_Init.vi"/>
			<Item Name="TortoiseGit_Global_Interface.vi" Type="VI" URL="../TortoiseGit/TortoiseGit_Global_Interface.vi"/>
			<Item Name="TortoiseGit_Global_OnCommand.vi" Type="VI" URL="../TortoiseGit/TortoiseGit_Global_OnCommand.vi"/>
			<Item Name="TortoiseGit_Global_OnUpdateCommand.vi" Type="VI" URL="../TortoiseGit/TortoiseGit_Global_OnUpdateCommand.vi"/>
			<Item Name="TortoiseGit_Item_Init.vi" Type="VI" URL="../TortoiseGit/TortoiseGit_Item_Init.vi"/>
			<Item Name="TortoiseGit_Item_Interface.vi" Type="VI" URL="../TortoiseGit/TortoiseGit_Item_Interface.vi"/>
			<Item Name="TortoiseGit_Item_OnCommand.vi" Type="VI" URL="../TortoiseGit/TortoiseGit_Item_OnCommand.vi">
				<Property Name="GIT_TOPLEVEL" Type="Str">C:\Users\jdonaldson.MRIGLOBAL\Documents\Labview Projects\TortoiseGit</Property>
			</Item>
			<Item Name="TortoiseGit_Item_OnPopupMenu.vi" Type="VI" URL="../TortoiseGit/TortoiseGit_Item_OnPopupMenu.vi"/>
			<Item Name="TortoiseGit_Item_NotifyChanged.vi" Type="VI" URL="../TortoiseGit/TortoiseGit_Item_NotifyChanged.vi"/>
			<Item Name="TortoiseGit_Provider_InitItems.vi" Type="VI" URL="../TortoiseGit/TortoiseGit_Provider_InitItems.vi"/>
			<Item Name="TortoiseGit_Provider_Interface.vi" Type="VI" URL="../TortoiseGit/TortoiseGit_Provider_Interface.vi"/>
			<Item Name="TortoiseGit_Provider_OnCommand.vi" Type="VI" URL="../TortoiseGit/TortoiseGit_Provider_OnCommand.vi"/>
			<Item Name="TortoiseGit_Provider_NotifyChanged.vi" Type="VI" URL="../TortoiseGit/TortoiseGit_Provider_NotifyChanged.vi"/>
			<Item Name="TortoiseGit_Provider_OnPopupMenu.vi" Type="VI" URL="../TortoiseGit/TortoiseGit_Provider_OnPopupMenu.vi"/>
			<Item Name="TortoiseGit_Provider_LoadComplete.vi" Type="VI" URL="../TortoiseGit/TortoiseGit_Provider_LoadComplete.vi"/>
			<Item Name="TortoiseGit_Provider_Startup.vi" Type="VI" URL="../TortoiseGit/TortoiseGit_Provider_Startup.vi"/>
			<Item Name="TortoiseGit_Provider_OnSaveProject.vi" Type="VI" URL="../TortoiseGit/TortoiseGit_Provider_OnSaveProject.vi"/>
			<Item Name="TortoiseGit_Provider_Shutdown.vi" Type="VI" URL="../TortoiseGit/TortoiseGit_Provider_Shutdown.vi"/>
			<Item Name="TortoiseGit_signed.ini" Type="Document" URL="../GProviders/TortoiseGit_signed.ini"/>
		</Item>
		<Item Name="Support" Type="Folder">
			<Property Name="GIT_FILE" Type="Bool">false</Property>
			<Property Name="type" Type="Str">tortoisegit</Property>
			<Item Name="Restart Project" Type="Folder">
				<Property Name="GIT_FILE" Type="Bool">false</Property>
				<Property Name="NI.SortType" Type="Int">3</Property>
				<Item Name="Load Project(Console&amp;Timer).vi" Type="VI" URL="../TortoiseGit/Support/Load Project(Console&amp;Timer).vi"/>
				<Item Name="ShutdownProject.vi" Type="VI" URL="../TortoiseGit/Support/ShutdownProject.vi"/>
				<Item Name="ReloadProject.vi" Type="VI" URL="../TortoiseGit/Support/ReloadProject.vi"/>
			</Item>
			<Item Name="SCC Drivers" Type="Folder">
				<Property Name="GIT_FILE" Type="Bool">false</Property>
				<Item Name="Abstract" Type="Folder">
					<Property Name="GIT_FILE" Type="Bool">false</Property>
					<Item Name="SCDriver_Abstract.lvclass" Type="LVClass" URL="../TortoiseGit/Support/SCDriver_Abstract/SCDriver_Abstract/SCDriver_Abstract.lvclass"/>
				</Item>
				<Item Name="GitDriver" Type="Folder">
					<Property Name="GIT_FILE" Type="Bool">false</Property>
					<Property Name="type" Type="Str">tortoisegit</Property>
					<Item Name="GitDriver.lvclass" Type="LVClass" URL="../TortoiseGit/Support/GitDriver/GitDriver.lvclass">
						<Property Name="type" Type="Str">tortoisegit</Property>
					</Item>
				</Item>
				<Item Name="TortoiseGit Driver" Type="Folder">
					<Property Name="GIT_FILE" Type="Bool">false</Property>
					<Property Name="type" Type="Str">tortoisegit</Property>
					<Item Name="TortoiseGitDriver.lvclass" Type="LVClass" URL="../TortoiseGit/Support/TortoiseGitDriver/TortoiseGitDriver.lvclass">
						<Property Name="type" Type="Str">tortoisegit</Property>
					</Item>
				</Item>
			</Item>
			<Item Name="TortoiseGitSupport" Type="Folder">
				<Property Name="GIT_FILE" Type="Bool">false</Property>
				<Property Name="GIT_TOPLEVEL" Type="Str">C:\Users\jdonaldson.MRIGLOBAL\Documents\Labview Projects\TortoiseGit</Property>
				<Item Name="Build Git Item Menu.vi" Type="VI" URL="../TortoiseGit/Support/Build Git Item Menu.vi"/>
				<Item Name="Build Git Provider Menu.vi" Type="VI" URL="../TortoiseGit/Support/Build Git Provider Menu.vi"/>
				<Item Name="CallSCCAction.vi" Type="VI" URL="../TortoiseGit/Support/CallSCCAction.vi"/>
				<Item Name="CheckGitProjectPath.vi" Type="VI" URL="../TortoiseGit/Support/CheckGitProjectPath.vi"/>
				<Item Name="FindTopLevelFolderPath.vi" Type="VI" URL="../TortoiseGit/Support/FindTopLevelFolderPath.vi"/>
				<Item Name="BuildOverlayStateCluster.vi" Type="VI" URL="../TortoiseGit/Support/BuildOverlayStateCluster.vi"/>
				<Item Name="FullGitUpdate.vi" Type="VI" URL="../TortoiseGit/Support/FullGitUpdate.vi"/>
				<Item Name="GitIconLabels.ctl" Type="VI" URL="../TortoiseGit/Support/GitIconLabels.ctl">
					<Property Name="GIT_TOPLEVEL" Type="Str">C:\Users\jdonaldson.MRIGLOBAL\Documents\Labview Projects\TortoiseGit</Property>
				</Item>
				<Item Name="GitStatusUpdate.vi" Type="VI" URL="../TortoiseGit/Support/GitStatusUpdate.vi"/>
				<Item Name="GitStatusUpdateSingleFile.vi" Type="VI" URL="../TortoiseGit/Support/GitStatusUpdateSingleFile.vi"/>
				<Item Name="CheckProjectForGit.vi" Type="VI" URL="../TortoiseGit/Support/CheckProjectForGit.vi"/>
				<Item Name="UpdateIconSingleFile.vi" Type="VI" URL="../TortoiseGit/Support/UpdateIconSingleFile.vi"/>
				<Item Name="Command Wrapper.vi" Type="VI" URL="../TortoiseGit/Support/Command Wrapper.vi"/>
				<Item Name="OnUpdateCommand_ASYNC.vi" Type="VI" URL="../TortoiseGit/Support/OnUpdateCommand_ASYNC.vi"/>
				<Item Name="UpdateFolderIcons.vi" Type="VI" URL="../TortoiseGit/Support/UpdateFolderIcons.vi"/>
				<Item Name="ProjectUpdateDameon.vi" Type="VI" URL="../TortoiseGit/Support/ProjectUpdateDameon.vi"/>
				<Item Name="GetCurrentBranchTag.vi" Type="VI" URL="../TortoiseGit/Support/GetCurrentBranchTag.vi"/>
				<Item Name="CompareBranchNames.vi" Type="VI" URL="../TortoiseGit/Support/CompareBranchNames.vi"/>
				<Item Name="BranchChangeReload.vi" Type="VI" URL="../TortoiseGit/Support/BranchChangeReload.vi"/>
				<Item Name="WriteBranchName.vi" Type="VI" URL="../TortoiseGit/Support/WriteBranchName.vi"/>
				<Item Name="UpdateBranchName.vi" Type="VI" URL="../TortoiseGit/Support/UpdateBranchName.vi"/>
				<Item Name="NewFileCheck.vi" Type="VI" URL="../TortoiseGit/Support/NewFileCheck.vi"/>
			</Item>
			<Item Name="TGit Setup" Type="Folder">
				<Property Name="GIT_FILE" Type="Bool">false</Property>
				<Item Name="Support" Type="Folder">
					<Property Name="GIT_FILE" Type="Bool">false</Property>
					<Item Name="RefreshTime.vi" Type="VI" URL="../TortoiseGit/Support/TGit Setup/RefreshTime.vi"/>
					<Item Name="SettingsFGControl.ctl" Type="VI" URL="../TortoiseGit/Support/TGit Setup/SettingsFGControl.ctl"/>
					<Item Name="SettingsCluster.ctl" Type="VI" URL="../TortoiseGit/Support/TGit Setup/SettingsCluster.ctl"/>
					<Item Name="SettingsFG.vi" Type="VI" URL="../TortoiseGit/Support/TGit Setup/SettingsFG.vi"/>
					<Item Name="Enabled.vi" Type="VI" URL="../TortoiseGit/Support/TGit Setup/Enabled.vi"/>
					<Item Name="GitPath.vi" Type="VI" URL="../TortoiseGit/Support/TGit Setup/GitPath.vi"/>
					<Item Name="GitBASHPath.vi" Type="VI" URL="../TortoiseGit/Support/TGit Setup/GitBASHPath.vi"/>
					<Item Name="Get_ReloadTimer.vi" Type="VI" URL="../TortoiseGit/Support/TGit Setup/Get_ReloadTimer.vi"/>
					<Item Name="TGitPath.vi" Type="VI" URL="../TortoiseGit/Support/TGit Setup/TGitPath.vi"/>
					<Item Name="BuildConfigPath.vi" Type="VI" URL="../TortoiseGit/Support/TGit Setup/BuildConfigPath.vi"/>
					<Item Name="Timer.vi" Type="VI" URL="../TortoiseGit/Support/TGit Setup/Timer.vi"/>
					<Item Name="FilterFolderPaths.vi" Type="VI" URL="../TortoiseGit/Support/FilterFolderPaths.vi"/>
				</Item>
				<Item Name="SetPreferences.vi" Type="VI" URL="../TortoiseGit/Support/TGit Setup/SetPreferences.vi"/>
			</Item>
			<Item Name="Git Bash" Type="Folder">
				<Property Name="GIT_FILE" Type="Bool">false</Property>
				<Item Name="GitBash.vi" Type="VI" URL="../TortoiseGit/Support/Git Bash/GitBash.vi"/>
			</Item>
		</Item>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Edit LVProj.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/EditLVProj/Edit LVProj.lvlib"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="FormatTime String.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/ElapsedTimeBlock.llb/FormatTime String.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="subElapsedTime.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/ElapsedTimeBlock.llb/subElapsedTime.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
			</Item>
			<Item Name="Global_Interface.ctl" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/Global_Interface.ctl"/>
			<Item Name="Item_Interface.ctl" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/Item_Interface.ctl"/>
			<Item Name="ItemRef.ctl" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/ItemRef.ctl"/>
			<Item Name="mxLvAddIconOverlays.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/mxLvAddIconOverlays.vi"/>
			<Item Name="mxLvChangeType.ctl" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/mxLvChangeType.ctl"/>
			<Item Name="mxLvDebugDisplayCaller.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/mxLvDebugDisplayCaller.vi"/>
			<Item Name="mxLvErrorHandler.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/mxLvErrorHandler.vi"/>
			<Item Name="mxLvGetFilePath.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/mxLvGetFilePath.vi"/>
			<Item Name="mxLvGetFilePathBatch.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/mxLvGetFilePathBatch.vi"/>
			<Item Name="mxLvGetItem.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/mxLvGetItem.vi"/>
			<Item Name="mxLvGetItemRef.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/mxLvGetItemRef.vi"/>
			<Item Name="mxLvGetNIIM.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/mxLvGetNIIM.vi"/>
			<Item Name="mxLvGetProjectRef.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/mxLvGetProjectRef.vi"/>
			<Item Name="mxLvMenuItem.ctl" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/mxLvMenuItem.ctl"/>
			<Item Name="mxLvNIIM.ctl" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/mxLvNIIM.ctl"/>
			<Item Name="mxLvOverlayAlias.ctl" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/mxLvOverlayAlias.ctl"/>
			<Item Name="mxLvOverlayState.ctl" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/mxLvOverlayState.ctl"/>
			<Item Name="mxLvProvider.mxx" Type="Document" URL="/&lt;resource&gt;/Framework/Providers/mxLvProvider.mxx"/>
			<Item Name="mxLvSetIconOverlaysBatch.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/mxLvSetIconOverlaysBatch.vi"/>
			<Item Name="mxLvSetPopupMenu.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/mxLvSetPopupMenu.vi"/>
			<Item Name="mxLvSetPopupMenuBatch.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/mxLvSetPopupMenuBatch.vi"/>
			<Item Name="mxLvUpdateUI.ctl" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/mxLvUpdateUI.ctl"/>
			<Item Name="mxLvUpdateUIBatch.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/mxLvUpdateUIBatch.vi"/>
			<Item Name="provcom_GetObjectItemFromProjectItem.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/Common/provcom_GetObjectItemFromProjectItem.vi"/>
			<Item Name="Provider_Interface.ctl" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/Provider_Interface.ctl"/>
			<Item Name="mxLvSetMenu.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/mxLvSetMenu.vi"/>
			<Item Name="mxLvSetIconOverlays.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/mxLvSetIconOverlays.vi"/>
			<Item Name="mxLvUpdateUI.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/mxLvUpdateUI.vi"/>
			<Item Name="mxLvNotifyChanged.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/mxLvNotifyChanged.vi"/>
			<Item Name="mxLvSetName.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/mxLvSetName.vi"/>
			<Item Name="mxLvSetToolbar.vi" Type="VI" URL="/&lt;resource&gt;/Framework/Providers/API/mxLvSetToolbar.vi"/>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Property Name="GIT_FILE" Type="Bool">false</Property>
		</Item>
	</Item>
</Project>
